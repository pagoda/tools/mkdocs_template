# Data Privacy

In itself, [Material for MkDocs][mkdocs_material] theme use for this
documentation does not perform any tracking and should adhere to the General
Data Protection Regulation (GDPR).

Moreover, no third-party services are used in this documentation, i.e. it does
not use google-fonts, google-analytics, neither Disqus.

Any rendering tools, such as [tablesort][tablesort], etc. are served locally
without using CDN or call to external website. The aims are to :

- Render the documentation with stable tools (i.e. documentation may not be
  using latest version of previously mentionned tools),
- Avoid useless network requests to external CDN,
- Render the documentation even if the user  has web browser add-on which filter
  javascript such as [uMatrix][uMatrix].

[mkdocs_material]: https://squidfunk.github.io/mkdocs-material/data-privacy/
[tablesort]: https://cdnjs.cloudflare.com/ajax/libs/tablesort/5.2.1/tablesort.min.js
[uMatrix]: https://github.com/gorhill/uMatrix
