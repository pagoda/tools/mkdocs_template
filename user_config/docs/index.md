---
hide:
  - navigation # Hide navigation
  - toc        # Hide table of contents
---

{% set curr_repo=subs("mkdocs_template") %}

<!-- BEGIN MKDOCS TEMPLATE -->
<!--
WARNING, DO NOT UPDATE CONTENT BETWEEN MKDOCS TEMPLATE TAG !
Modified content will be overwritten when updating
-->

<div align="center">

  <!-- Project Title -->
  <a href="{{ git_platform.url }}{{ curr_repo.repo_path_with_namespace }}">
    <img src="{{ curr_repo.logo }}" width="200px">
    <h1>{{ curr_repo.name }}</h1>
  </a>

<hr>

{{ to_html(curr_repo.desc) }}

<hr>

</div>

--------------------------------------------------------------------------------

<!-- END MKDOCS TEMPLATE -->
